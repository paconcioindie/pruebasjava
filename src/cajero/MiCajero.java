
package cajero;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class MiCajero {
    
    private final ArrayList<Integer> billetesValidos = new ArrayList(
        Arrays.asList(10, 20, 50, 100, 200, 500, 1000)
    );
    
    public void depositar(ArrayList<Integer> billetes) {
        System.out.println("--- Entro a depositar");
        
        if(!validarBilletes(billetes)) {
            System.out.println("Billetes no válidos");
            return;
        }
        
        int totalImporte = contarBilletes(billetes);
        System.out.println("Se depositaron $" + totalImporte);
    }
    
    public void retirar(int importe) {
        System.out.println("+++ Entro a retirar");
        
        if(!validarImporte(importe)) {
            System.out.println("El importe no es válido");
            return;
        }
        
        ArrayList<Integer> billetes = obtenerBilletes(importe);
        System.out.println("billetes a retirar = " + billetes);
    }
    
    private boolean validarBillete(int valor) {
        return billetesValidos.contains(valor);
    }
    
    private boolean validarBilletes(ArrayList<Integer> billetes) {
        for(int billete : billetes) {
            if(!validarBillete(billete)) {
                return false;
            }
        }
        return true;
    }
    
    private int contarBilletes(ArrayList<Integer> billetes) {
        int total = 0;
        for(int billete : billetes) {
            total += billete;
        }
        return total;
    }
    
    private boolean validarImporte(int importe) {
        if(importe % 10 != 0) {
            return false;
        }
        return true;
    }
    
    private ArrayList<Integer> obtenerBilletes(int importe) {
        ArrayList<Integer> billetes = new ArrayList();
        
        Collections.sort(billetesValidos, Collections.reverseOrder());
        
        for(int billeteValido : billetesValidos) {
            int cantBilletes = importe / billeteValido;
            
            for(int i=1; i<=cantBilletes; i++) {
                billetes.add(billeteValido);
            }
            
            int resto = importe % billeteValido;

            importe = resto;
        }
        
        return billetes;
    }
    
    public static void main(String[] args) {
        System.out.println("*** mi cajero");
        
        MiCajero miCajero = new MiCajero();
        ArrayList<Integer> billetes = new ArrayList(
            Arrays.asList(10, 10, 20, 50, 100, 100)
        );
        miCajero.depositar(billetes);
        
        ArrayList<Integer> billetes2 = new ArrayList(
            Arrays.asList(10, 74, 20, 51, 100, 100)
        );
        miCajero.depositar(billetes2);
        
        int importe = 328;
        miCajero.retirar(importe);
        
        int importe2 = 770;
        miCajero.retirar(importe2);
        
        ArrayList<Integer> billetes3 = new ArrayList(
            Arrays.asList(1000, 500, 500, 50, 50, 50, 20, 20)
        );
        miCajero.depositar(billetes3);
        
        miCajero.retirar(1520);
    }
    
}
