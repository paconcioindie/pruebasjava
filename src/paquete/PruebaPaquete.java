
package paquete;


public class PruebaPaquete {
    
    public void metodoPublico() {
        System.out.println("entro a metodoPublico");
    }
    
    private void metodoPrivado() {
        System.out.println("entro a metodoPrivado");
    }
    
    protected void metodoProtegido() {
        System.out.println("entro a metodoProtegido");
    }
    
    public static void main(String[] args) {
        System.out.println(
            "- Este es el main de MiPaquete de la clase PruebaPaquete"
        );
        
        PruebaPaquete pruebaPaquete = new PruebaPaquete();
        pruebaPaquete.metodoPublico();
        pruebaPaquete.metodoPrivado();
        pruebaPaquete.metodoProtegido();
    }
}
