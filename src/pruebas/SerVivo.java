
package pruebas;


public abstract class SerVivo implements Ser {
    
    @Override
    public abstract void alimentarse();
    
    @Override
    public abstract void moverse();
    
    @Override
    public void vivir() {
        System.out.println("El ser vivo vive");
    }
    
    @Override
    public void sentir() {
        System.out.println("El ser vivo siente");
    }
    
}
