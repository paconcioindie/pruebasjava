
package pruebas;


public class Fibonacci {
    
    public int[] getSecuencia(int cantValores) {
        System.out.println("mostrar la serie de fibonacci");
        
        int[] secuencia = new int[cantValores];
        
        for(int i=0; i < cantValores; i++) {
            if(i == 0) {
                secuencia[i] = i;
            } else if(i == 1) {
                secuencia[i] = secuencia[i-1] + i;
            } else {
                secuencia[i] = secuencia[i-2] + secuencia[i-1];
            }
        }
        
        return secuencia;
    }
    
}
