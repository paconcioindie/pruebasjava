
package pruebas;


public class AnimalHerbivoro extends Animal {
    
    @Override
    public void alimentarse() {
        System.out.println("El animal herbivoro come plantas");
    }
    
    @Override
    public void moverse() {
        System.out.println("El animal herbivor puede moverse");
    }
    
    @Override
    public void sentir() {
        System.out.println("El animal herbivor siente");
    }
    
}
