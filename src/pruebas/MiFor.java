
package pruebas;

import java.util.ArrayList;
import java.util.Iterator;


public class MiFor {
    
    public void recorrerNumeros() {
        System.out.println("*** recorrerNumeros");
        
        int[] listaNumeros = new int[6];
        listaNumeros[0] = 3;
        listaNumeros[1] = 18;
        listaNumeros[2] = 1;
        listaNumeros[5] = 100;
        
        for(int i=0; i< listaNumeros.length; i++) {
            System.out.println("numero = " + listaNumeros[i]);
        }
    }
    
    public void recorrerPalabras() {
        System.out.println("*** recorrerPalabras");
        
        String[] listaPalabras = {"hola", "chau", "como va"};
        
        for(String palabra : listaPalabras) {
            System.out.println("palabra = " + palabra);
        }
    }
    
    public void recorrerArrayList() {
        System.out.println("*** recorrerArrayList");
        
        ArrayList<Integer> arrayListInteger = new ArrayList();
        
        arrayListInteger.add(96);
        
        arrayListInteger.add(35);
        
        arrayListInteger.add(1088);
        
        arrayListInteger.add(17);
        
        arrayListInteger.add(5);
        
        arrayListInteger.add(3, 8);
        
        int numElementos = arrayListInteger.size();
        System.out.println("numElementos = " + numElementos);
        
        int valorDos = arrayListInteger.get(2);
        System.out.println("Valor del elemento 2 de la lista = " + valorDos);
        
        boolean contieneValor = arrayListInteger.contains(35);
        System.out.println("Contiene el valor 35? " + contieneValor);
        
        boolean contieneValor2 = arrayListInteger.contains(77);
        System.out.println("Contiene el valor 77? " + contieneValor2);
        
        int pos = arrayListInteger.indexOf(96);
        System.out.println("Posición del 96 = " + pos);
        
        boolean arrayVacio = arrayListInteger.isEmpty();
        System.out.println("El array está vacío? " + arrayVacio);
        
        ArrayList<String> arrayListString = new ArrayList();
        
        boolean arrayVacio2 = arrayListString.isEmpty();
        System.out.println("El array 2 está vacío? " + arrayVacio2);
        
        Iterator<Integer> iterador = arrayListInteger.iterator();
        while(iterador.hasNext()) {
            int numero = iterador.next();
            System.out.println("numero array list = " + numero);
        }
        
        arrayListInteger.forEach((numero2) -> {
            System.out.println("numero 2 = " + numero2);
        });
    }
    
}
