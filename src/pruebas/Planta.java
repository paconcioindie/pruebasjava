
package pruebas;


public class Planta extends SerVivo {

    @Override
    public void alimentarse() {
        System.out.println("La planta se alimenta a través de la fotosíntesis");
    }

    @Override
    public void moverse() {
        System.out.println("La planta no puede moverse");
    }
    
    @Override
    public void vivir() {
        System.out.println("La planta vive");
    }
    
    public void florecer() {
        System.out.println("La planta puede florecer");
    }
    
}
