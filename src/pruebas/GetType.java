
package pruebas;


public class GetType {
    
    public void obtenerDistintosTipos() {
        String nombre = "Juan";
        System.out.println("tipo de la variable nombre = " + nombre.getClass().getName());
        
        Boolean bandera = true;
        System.out.println("tipo de la variable bandera = " + bandera.getClass().getName());
    }
    
}
