
package pruebas;


public class AnimalCarnivoro extends Animal {
    
    @Override
    public void alimentarse() {
        System.out.println("El animal carnivoro come carne");
    }
    
    @Override
    public void moverse() {
        System.out.println("El animal carnivoro puede moverse");
    }
    
}
