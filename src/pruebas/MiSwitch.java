
package pruebas;


public class MiSwitch {
    
    public void probarSwitch(int caso) {
        System.out.println("*** probarSwitch");
        
        switch(caso) {
            case 5:
                System.out.println("caso 5");
                break;
            case 17:
                System.out.println("caso 17");
                break;
            default:
                System.out.println("entro al default");
        }
    }
}
