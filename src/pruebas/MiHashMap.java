
package pruebas;

import java.util.HashMap;


public class MiHashMap {
    
    public void crearHashMap() {
        HashMap<String, String> miMap = new HashMap();
        miMap.put("nombre", "Juan");
        miMap.put("apellido", "Riabiz");
        
        String nombre = miMap.get("nombre");
        System.out.println("nombre = " + nombre);
        
        for(String key : miMap.keySet()) {
            System.out.println("key = " + key);
            System.out.println("valor = " + miMap.get(key));
        }
    }
    
}
