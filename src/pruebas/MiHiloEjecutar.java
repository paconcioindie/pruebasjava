
package pruebas;


public class MiHiloEjecutar {
    
    //@Override
    //public void hiloTerminado() {
        //System.out.println("* termino el hilo");
    //}
    
    public static void main(String[] args) {
        System.out.println("+++ MiHilo");
        MiHiloInterface miHiloInterface = new MiHiloInterface() {
            @Override
            public void hiloTerminado() {
                System.out.println("* termino el hilo");
            }
        };
        MiHilo miHilo = new MiHilo(miHiloInterface);
        miHilo.start();
        System.out.println("esto se ejecuta mientras sigue el hilo");
    }
    
}
