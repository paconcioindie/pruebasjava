
package pruebas;


public class MiHilo extends Thread {
    
    private MiHiloInterface miHiloInterface;
    
    public MiHilo(MiHiloInterface miHiloInterface) {
        this.miHiloInterface = miHiloInterface;
    }
    
    @Override
    public void run() {
        int contador = 1;
        while(contador <= 5) {
            contador++;
            System.out.println("estoy trabajando en el hilo");
            try {
                Thread.sleep(1000);
            } catch(InterruptedException ex) {
                System.out.println("Error en el sleep de MiHilo, ex = " + ex);
            }
        }
        miHiloInterface.hiloTerminado();
    }
    
}
