
package pruebas;

import java.io.*;


public class Archivo {
    
    public void leerArchivo() throws IOException {
        File archivo = new File("C:\\paconcio\\Descargas\\mi_archivo.txt");
        FileReader fr = new FileReader(archivo);
        BufferedReader br = new BufferedReader(fr);

        String linea;
        
        while((linea=br.readLine()) != null) {
            System.out.println(linea);
        }
        
    }
    
    public void escribirArchivo() throws IOException {
        FileWriter archivo = new FileWriter("C:\\paconcio\\Descargas\\mi_archivo.txt", true);
        
        PrintWriter pw = new PrintWriter(archivo);

        for(int i=0; i<10; i++) {
            pw.write("hola como va?\n");
        }
        
        pw.write("chau\n");
        
        archivo.close();
    }
    
}
