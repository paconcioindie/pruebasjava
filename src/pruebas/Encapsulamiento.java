
package pruebas;


public class Encapsulamiento {
    
    private int unNumero;
    public String unaPalabra = "casa";
    protected boolean bandera = true;
    
    public int getUnNumero() {
        return this.unNumero;
    }
    
    public void setUnNumero(int numero) {
        this.unNumero = numero;
    }
    
}
