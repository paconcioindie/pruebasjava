
package pruebas;

import java.io.IOException;

import paquete.PruebaPaquete;


public class Pruebas {
    
    public static void main(String[] args) {
        System.out.println("+++ Pruebas");
            
        System.out.println("+++ MiFor");
        MiFor miFor = new MiFor();
        miFor.recorrerNumeros();
        miFor.recorrerPalabras();
        miFor.recorrerArrayList();
        
        System.out.println("+++ MiSwitch");
        MiSwitch miSwitch = new MiSwitch();
        miSwitch.probarSwitch(5);
        miSwitch.probarSwitch(22);
        
        System.out.println("+++ Herencia");
        AnimalCarnivoro animalCarnivoro = new AnimalCarnivoro();
        animalCarnivoro.alimentarse();
        animalCarnivoro.moverse();
        animalCarnivoro.vivir();
        AnimalHerbivoro animalHerbivoro = new AnimalHerbivoro();
        animalHerbivoro.alimentarse();
        animalHerbivoro.moverse();
        animalHerbivoro.vivir();
        Planta planta = new Planta();
        planta.alimentarse();
        planta.moverse();
        planta.vivir();
        planta.florecer();
        
        System.out.println("+++ Polimorfimo");
        SerVivo perro = new AnimalCarnivoro();
        perro.alimentarse();
        perro.moverse();
        perro.vivir();
        SerVivo vaca = new AnimalHerbivoro();
        vaca.alimentarse();
        vaca.moverse();
        vaca.vivir();
        SerVivo rosas = new Planta();
        rosas.alimentarse();
        rosas.moverse();
        rosas.vivir();
        ((Planta)rosas).florecer();
        
        System.out.println("+++ Interface");
        SerVivo perro2 = new AnimalCarnivoro();
        perro2.alimentarse();
        perro2.moverse();
        perro2.vivir();
        perro2.sentir();
        SerVivo vaca2 = new AnimalHerbivoro();
        vaca2.alimentarse();
        vaca2.moverse();
        vaca2.vivir();
        vaca2.sentir();
        SerVivo rosas2 = new Planta();
        rosas2.alimentarse();
        rosas2.moverse();
        rosas2.vivir();
        rosas2.sentir();
        
        System.out.println("+++ Encapsulamiento");
        Encapsulamiento encapsulamiento = new Encapsulamiento();
        encapsulamiento.setUnNumero(7);
        int numero = encapsulamiento.getUnNumero();
        System.out.println("numero = " + numero);
        String palabra = encapsulamiento.unaPalabra;
        System.out.println("palabra = " + palabra);
        boolean bandera = encapsulamiento.bandera;
        System.out.println("bandera protected = " + bandera);
        
        System.out.println("+++ Fibonacci");
        Fibonacci fibonacci = new Fibonacci();
        int[] secuencia = fibonacci.getSecuencia(10);
        for(int numSecuencia : secuencia) {
            System.out.println("numSecuencia = " + numSecuencia);
        }
        
        System.out.println("+++ Archivo");
        Archivo archivo = new Archivo();
        try {
            archivo.leerArchivo();
        } catch(IOException ex) {
            System.out.println("error al leer el archivo = " + ex);
        }
        try {
            archivo.escribirArchivo();
        } catch(IOException ex) {
            System.out.println("error al escribir el archivo = " + ex);
        }
        
        System.out.println("+++ Paquete");
        PruebaPaquete pruebaPaquete = new PruebaPaquete();
        pruebaPaquete.metodoPublico();
        
        System.out.println("+++ HasMap");
        MiHashMap miHashMap = new MiHashMap();
        miHashMap.crearHashMap();
        
        System.out.println("+++ GetType");
        GetType getType = new GetType();
        getType.obtenerDistintosTipos();
        
        System.out.println("+++ Fechas");
        Fechas fechas = new Fechas();
        fechas.pruebasFechas();
    }
    
}
