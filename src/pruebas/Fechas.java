
package pruebas;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class Fechas {
    
    public void pruebasFechas() {
        Date fecha = new Date();
        
        System.out.println("fecha actual = " + fecha.toString());
        
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        System.out.println("fecha con formato = " + formato.format(fecha));
        
        Calendar calendar = Calendar.getInstance();
        
        calendar.add(Calendar.DAY_OF_YEAR, 35);
        
        Date fechaModificada = calendar.getTime();
        System.out.println("fecha modificada = " + fechaModificada);
        System.out.println(
            "fecha modificada con formato = " + formato.format(fechaModificada)
        );
        
        String miFecha = "1986-09-23 23:00:00";
        Date miFechaDate = null;
        try {
            miFechaDate = formato.parse(miFecha);
        } catch(ParseException ex) {
            System.out.println("El formato de fecha no es valido");
        }
        calendar.setTime(miFechaDate);
        System.out.println("fecha inventada = " + calendar.getTime());
        
        long difFechas = fecha.getTime() - miFechaDate.getTime();
        System.out.println("difFechas = " + difFechas);
        
        float diasDif = (difFechas / (1000*60*60*24));
        System.out.println("diasDif = " + diasDif);
        
        float aniosDif = (difFechas / (1000*60*60*24) / 365);
        System.out.println("aniosDif = " + aniosDif);
    }
    
}
