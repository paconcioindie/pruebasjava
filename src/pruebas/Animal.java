
package pruebas;


public abstract class Animal extends SerVivo {
    
    @Override
    public abstract void alimentarse();
    
    @Override
    public abstract void moverse();
    
}
